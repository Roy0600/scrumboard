let callUrl;
class APIGetCall {
    constructor(props) {
        callUrl = props;
    }

    fetchData(){
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Basic Um95aGVpbWVsQGhvdG1haWwuY29tOkJZSzZoWGU4alpoOTdWU1dYNEpQOUJDQw==");
        myHeaders.append("Cookie", "atlassian.xsrf.token=75ba9ad2-2d07-4f32-851e-87b8a8ee4cad_2834887903d3cc413ff0ba3d7e4aebc91b989f4d_lin");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };


        return fetch(callUrl, requestOptions)
    }

}
export default APIGetCall