import Highcharts from 'highcharts';
import HighchartsReact from "highcharts-react-official";
import React, {Component} from "react";
import APIGetCall from "./APIGetCall";
import './Styling/ProgressBar.css'



const options = {
    chart: {
        type: 'bar',
        height: 120
    },
    title: null,
    subtitle: {
        text: null,
        verticalAlign: "bottom",
    },
    credits: false,
    legend: false,
    tooltip: false,
    plotOptions: {
        bar: {
            stacking: 'normal',
            borderWidth: 0,
            borderRadius: 6
        }
    },
    xAxis: {
        visible: false
    },
    yAxis: {
        visible: false,
        min: 0,
        max: 100,
        title: {
            text: null
        },
        gridLineWidth: 0,
        labels: {
            y: -2
        }
    },

    series: [
        {
            name: "Fill",
            data: [],
            color: "gray",
            grouping: false,
            enableMouseTracking: false
        },
        {
            name: "Done",
            data: [],
            color: '#159B39',
            enableMouseTracking: false,
            dataLabels: {
                enabled: true,
                inside: true,
                align: 'center',
                format: '{point.y:.2f}% <br/> {series.name}',
                style: {
                    color: 'white',
                    textOutline: false,
                }
            }
        },
        {
            name: "InternalTesting",
            data: [],
            color: '#8FC641',
            enableMouseTracking: false,
            dataLabels: {
                enabled: true,
                inside: true,
                align: 'center',
                format: '{point.y:.2f}% <br/> {series.name}',
                style: {
                    color: 'white',
                    textOutline: false,
                }
            }
        },
        {
            name: "PRReview",
            data: [],
            color: '#ff8d32',
            enableMouseTracking: false,
            dataLabels: {
                enabled: true,
                inside: true,
                align: 'center',
                format: '{point.y:.2f}% <br/> {series.name}',
                style: {
                    color: 'white',
                    textOutline: false,
                }
            }
        },
        {
            name: "InProgress",
            data: [],
            color: '#E95A3C',
            enableMouseTracking: false,
            dataLabels: {
                enabled: true,
                inside: true,
                align: 'center',
                format: '{point.y:.2f}% <br/> {series.name}',
                style: {
                    color: 'white',
                    textOutline: false,
                }
            }
        },
        {
            name: "Todo",
            data: [],
            color: '#DA221E',
            enableMouseTracking: false,
            dataLabels: {
                enabled: true,
                inside: true,
                align: 'center',
                format: "{point.y:.2f}% <br/> {series.name}",
                style: {
                    color: 'white',
                    textOutline: false,
                }
            }
        }
    ]
}


class ProgressBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };

    }

    componentDidMount() {
        let stats;
        if(this.props.name === "Main") {
            stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints()");
        }
        else{
            stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=project = "+this.props.name+"  AND sprint in openSprints() ")
        }
        console.log(this.props.name)
        stats.fetchData()
            .then(response => response.json())
            .then((result) => {
                    let Total = null;
                    let ToDo = null;
                    let InProgress = null;
                    let PRReview = null;
                    let InternalTesting = null;
                    let Done = null;


                    for (let i = 0; i < result.issues.length; i++) {
                        if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                            Total += result.issues[i].fields.aggregatetimeoriginalestimate;
                        }
                        if (result.issues[i].fields.status.name === "To Do") {
                            if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                                ToDo += result.issues[i].fields.aggregatetimeoriginalestimate;
                            }
                        }
                        if (result.issues[i].fields.status.name === "In Progress") {
                            if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                                InProgress += result.issues[i].fields.aggregatetimeoriginalestimate;
                            }
                        }
                        if (result.issues[i].fields.status.name === "PR Review") {
                            if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                                PRReview += result.issues[i].fields.aggregatetimeoriginalestimate;
                            }
                        }
                        if (result.issues[i].fields.status.name === "Internal Testing") {
                            if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                                InternalTesting += result.issues[i].fields.aggregatetimeoriginalestimate;
                            }
                        }

                        if (result.issues[i].fields.status.name === "Done") {
                            if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                                Done += result.issues[i].fields.aggregatetimeoriginalestimate;
                            }
                        }
                    }
                    this.setState({
                        isLoaded: true,
                        posts: [Total, Done, InternalTesting, PRReview, InProgress, ToDo]
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }


    render() {
        const {error, isLoaded, posts} = this.state;

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            console.log(posts)
            options.series[0].data = [posts[0]]; //Total
            if (posts[1] !== null) {
                options.series[1].data = [posts[1] / posts[0] * 100];//Done
            }
            if (posts[2] !== null) {
                options.series[2].data = [posts[2] / posts[0] * 100];//Internal Testing
            }
            if (posts[3] !== null) {
                options.series[3].data = [posts[3] / posts[0] * 100];//PR Review
            }
            if (posts[4] !== null) {
                options.series[4].data = [posts[4] / posts[0] * 100];//In Progress
            }
            if (posts[5] !== null) {
                options.series[5].data = [posts[5] / posts[0] * 100];//To Do
            }


            return (
                <div className = "boxProgressBar">
                <a>Sprint Progressbar</a>
                    <div className="progressBar">
                        <HighchartsReact highcharts={Highcharts} options={options}/>
                    </div>
                </div>
            )
        }
    }

}

export default ProgressBar
