import React, {Component} from 'react';
import APIGetCall from "./APIGetCall";
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Team from "../Pages/Team"
import Home from "../Pages/Home";
import * as AiIcons from "react-icons/ai";
import {IconContext} from "react-icons";
import './Styling/SideMenu.css';
import './Styling/App.css';
import Logo from '../Components/Styling/Images/DharmaLogo.svg';
import * as FaIcons from 'react-icons/fa';
import PersonalPage from "../Pages/PersonalPage";
//
class APIGetTeams extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }

    componentDidMount(props) {
        let Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/project");
        Call.fetchData()
            .then(response => response.json())
            .then((result) => {
                    this.setState({
                        isLoaded: true,
                        posts: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, posts} = this.state;

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            let teams = [];
            for (let i = 0; i < posts.valueOf().length; i++) {
                teams.push({id: posts[i].id, key: posts[i].key, name: posts[i].name});
            }
            console.log(teams)


            return (
                <IconContext.Provider value={{color: '#Red'}}>
                    <nav className='nav-menu'>
                        <BrowserRouter>
                            <nav className='nav-text'>

                                <ul>
                                    <div className="dharmaLogo">
                                        <img src={Logo} alt="Dharma Logo"/>
                                    </div>
                                    <li onClick="window.location.reload();">

                                        <Link to="/"><AiIcons.AiFillHome/>
                                            <a>Home</a>
                                        </Link>

                                    </li>
                                    {teams.map((item) => {
                                        return (
                                            <Link>
                                                <li onClick="window.location.reload();">
                                                    {/*<div className = "sideMenuTeams"><FaIcons.FaStackOverflow/></div>*/}
                                                    {/*<div className="sideMenuTeams">*/}
                                                        <Link to={"/team/" + item.key}><FaIcons.FaStackOverflow/>
                                                            <a>{item.name}</a>
                                                        </Link>
                                                    {/*</div>*/}
                                                </li>
                                            </Link>
                                        )
                                    })}
                                    {/*<Link to= "/personalpage"><a>Personal Page</a></Link>*/}
                                </ul>
                            </nav>
                            <Switch>
                                <Route path='/' exact component={Home}/>
                                <Route path='/personalpage' exact component={PersonalPage}/>
                                {teams.map((item) => {
                                    return (
                                        <Route path={"/team/" + item.key}>
                                            <span key={item.id}><Team key={item.key} data={item}></Team></span>
                                        </Route>
                                    )
                                })}
                            </Switch>

                        </BrowserRouter>
                    </nav>
                </IconContext.Provider>
            );
        }
    }
}

export default APIGetTeams