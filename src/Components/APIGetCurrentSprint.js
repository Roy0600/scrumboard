import React , { Component } from 'react'
import './Styling/CurrentSprint.css'

class APIGetCurrentSprint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }

//
    componentDidMount(props) {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Basic Um95aGVpbWVsQGhvdG1haWwuY29tOkJZSzZoWGU4alpoOTdWU1dYNEpQOUJDQw==");
        myHeaders.append("Cookie", "atlassian.xsrf.token=75ba9ad2-2d07-4f32-851e-87b8a8ee4cad_2834887903d3cc413ff0ba3d7e4aebc91b989f4d_lin");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

//
        fetch("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?&jql=sprint in openSprints()", requestOptions)
            .then(response => response.json())
            .then((result) => {
                    this.setState({
                        isLoaded: true,
                        posts: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, posts} = this.state;

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            let sprintName
            sprintName = posts.issues[0].fields.customfield_10020[0].name;

            return (sprintName)
        }

    }
}

export default APIGetCurrentSprint