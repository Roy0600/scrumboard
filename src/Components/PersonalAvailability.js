import React, {Component} from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
import './Styling/PersonalAvailability.css'
import PersonalData from './PersonalData.json';

const options = {
    chart: {
        type: 'column',
        color: '#FFFFFF',
        height: 200,
    },
    credits: {
        enabled: false
    },


    title: {
        text: 'Personal Availability',
        style: {"color": '#ffffff'}
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',

        ],
        labels: {
            style: {
                color: '#FFFFFF'
            }
        },
        crosshair: true,
        color: '#FFFFFF'
    },
    yAxis: {
        min: 8,
        max: 20,
        title: {
            text: 'Time in hours',
            color: '#FFFFFF'
        }
    },

    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    legend: false,
    tooltip: false,

    series: [{
        borderWidth: false,
        enableMouseTracking: false,
        name: 'Availability',
        data: [0,0,0,0,0,0,0],
        color: {
            linearGradient: {
                x1: 0,
                x2: 0,
                y1: 0,
                y2: 1
            },
            stops: [
                [0, '#0e3f99'],
                [1, '#46a7f3']
            ],
        },
        // color: '#3698dd',
        pointWidth: 30,


    }
    ]

}

class PersonalAvailability extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
        };

    }

    componentDidMount() {
        console.log(this.props.userId)
    }


    render() {
        for(let i = 0; i < PersonalData.length; i++ ) {
            if (this.props.userId === PersonalData[i].userId) {
                console.log(PersonalData[i].availableTime)
                 options.series[0].data = PersonalData[i].availableTime;
            }
        }
        return (
            <div className="boxPersonalAvailability">
                <div className="PersonalAvailability">
                    <HighchartsReact highcharts={Highcharts} options={options}/>
                </div>
            </div>
        )
    }


}

export default PersonalAvailability