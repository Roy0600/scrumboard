import React, {Component} from "react";
import './Styling/SprintGoal.css'
import APIGetCall from "./APIGetCall";
import PersonalData from "./PersonalData.json";

class SprintGoal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: [],
            goal: [],

        };
    }


    componentDidMount() {
        let stats;
        let data = [];

        let accountId = this.props.name.split("#");

        if (accountId[0] === "UserId") {

        } else {
            //zelfde als burndown
            if (this.props.name === "Main") {
                stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints()")
            } else {
                stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints()")
            }

            stats.fetchData()
                .then(response => response.json())
                .then((result) => {
                        let Total = 0;

                        for (let i = 0; i < result.issues.length; i++) {
                            if (result.issues[i].fields.project.id === this.props.name) {
                                data = result.issues[i].fields.customfield_10020[0].goal;
                                break;

                            }

                        }
                        this.setState({
                            isLoaded: true,
                            goal: data,
                        })
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        })
                    }
                )


        }
    }

    render() {
        const {error, isLoaded, posts, goal} = this.state;
        console.log(goal)
        let sprintGoal;
        let accountId = this.props.name.split("#");
        if (accountId[0] === "UserId") {
            for (let j = 0; j < PersonalData.length; j++) {
                if (accountId[1] === PersonalData[j].userId) {
                    sprintGoal = PersonalData[j].sprintgoal;
                }
            }
        } else {
            if (error) {
                return <div>error in loading</div>
            } else if (!isLoaded) {
                return <div>Loading.......</div>
            } else {

                sprintGoal = goal;
            }
        }


        return (
            <div className="boxSprintGoal">
                <div className="sprintGoal">
                    <h2>Sprint Goal</h2>
                    <textarea className="textBox" rows="5" cols="65">{sprintGoal}</textarea>
                </div>
            </div>
        )
    }

}


export default SprintGoal
