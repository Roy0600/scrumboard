import React from 'react'
import './Styling/ProgressStats.css'
import APIGetStatusAmount from "./APIGetStatusAmount";

const ProgressStats=(data)=> {


    console.log(data.name[0])
    console.log(data.name[1])
    return (
        <div className="containerProgressStats">
            <div className="box1">
                <div className="icon"></div>
                <div className="content">
                    <h3>To Do</h3>
                    <APIGetStatusAmount data={["To Do", data.name[0], data.name[1]]}/>
                </div>
            </div>
            <div className="box2">
                <div className="icon"></div>
                <div className="content">
                    <h3>In Progress</h3>
                    <APIGetStatusAmount data={["In Progress", data.name[0], data.name[1]]}/>
                </div>
            </div>
            <div className="box3">
                <div className="icon"></div>
                <div className="content">
                    <h3>PR review</h3>
                    <APIGetStatusAmount data={["PR Review", data.name[0], data.name[1]]}/>
                </div>
            </div>
            <div className="box4">
                <div className="icon"></div>
                <div className="content">
                    <h3>Internal testing</h3>
                    <APIGetStatusAmount data={["Internal Testing", data.name[0], data.name[1]]}/>
                </div>
            </div>
            <div className="box5">
                <div className="icon"></div>
                <div className="content">
                    <h3>Done</h3>
                    <APIGetStatusAmount data={["Done", data.name[0], data.name[1]]}/>
                </div>
            </div>
        </div>

    )
}

//
export default ProgressStats