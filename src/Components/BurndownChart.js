import Highcharts from 'highcharts';
import HighchartsReact from "highcharts-react-official";
import APIGetCall from "./APIGetCall";
import React, {Component} from "react";
import './Styling/BurndownChart.css'

//Functie voor data verwerking Team 1
function burndownChartDataTeam1(Monday1, Tuesday1, Wednesday1, Thursday1, Friday1, Saturday1,
                                Sunday1, Monday2, Tuesday2, Wednesday2, Thursday2, Friday2, Saturday2,
                                Sunday2) {

    //input hier eventueel omzetten
    return [Monday1, Tuesday1, Wednesday1, Thursday1, Friday1, Saturday1,
        Sunday1, Monday2, Tuesday2, Wednesday2, Thursday2, Friday2, Saturday2,
        Sunday2]
}

//Aanmaken burndownchart
class burndownChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            burnDownData: [],
            displayName: null,
            accountId: null
        };
    }

    componentDidMount() {
        let startDate;
        let burnDownData = [];
        let stats;


        console.log(this.props.name)

        let accountId = this.props.name.split("#");

        console.log("AccountID= " + accountId);

        if (accountId[0] === "UserId") {
            stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints() and assignee in (" + accountId[1] + ") ORDER BY statusCategoryChangedDate ASC")
        } else {
            if (this.props.name === "Main") {
                stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints() ORDER BY statusCategoryChangedDate ASC")
            } else {
                stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=project = " + this.props.name + " AND sprint in openSprints() ORDER BY statusCategoryChangedDate ASC")
            }
        }

        stats.fetchData()
            .then(response => response.json())
            .then((result) => {
                    startDate = result.issues[0].fields.customfield_10020[0].startDate;
                    startDate = startDate.split('T')[0];
                    let startYear = startDate.split('-')[0];
                    let startMonth = startDate.split('-')[1];
                    let startDay = startDate.split('-')[2];
                    startDate = startDate.split('-')[0] + startDate.split('-')[1] + startDate.split('-')[2];
                    console.log(startDate)
                    let displayName;


                    let currentDate = new Date();
                    let currentDay = (currentDate.getDate());
                    let currentMonth = (currentDate.getMonth() + 1);
                    let currentYear = currentDate.getFullYear();


                    let currentDayInSprint = (((((((currentYear - startYear) * 12) + currentMonth) - startMonth) * 31) + currentDay) - startDay)


                    let Total = 0;
                    for (let i = 0; i < result.issues.length; i++) {
                        if (result.issues[i].fields.aggregatetimeoriginalestimate != null) {
                            Total += result.issues[i].fields.aggregatetimeoriginalestimate;
                        }
                    }
                    let AlreadyCompleted = Total;

                    let dayNumber = 0;
                    while (dayNumber <= currentDayInSprint) {
                        console.log(currentDayInSprint)
                        burnDownData[dayNumber] = (AlreadyCompleted / Total * 100);
                        for (let j = 0; j < result.issues.length; j++) {
                            let selectedDate = result.issues[j].fields.statuscategorychangedate;
                            selectedDate = selectedDate.split('T')[0];


                            let selectedYear = parseInt(selectedDate.split('-')[0]);
                            let selectedMonth = parseInt(selectedDate.split('-')[1]);
                            let selectedDay = parseInt(selectedDate.split('-')[2]);

                            if ((((((((selectedYear - startYear) * 12) + selectedMonth) - startMonth) * 31) + selectedDay) - startDay) === dayNumber) {
                                if (result.issues[j].fields.status.name === "Done") {
                                    if (result.issues[j].fields.aggregatetimeoriginalestimate != null) {
                                        burnDownData[dayNumber] -= (result.issues[j].fields.aggregatetimeoriginalestimate / Total * 100);
                                        AlreadyCompleted -= result.issues[j].fields.aggregatetimeoriginalestimate;
                                    }
                                }
                            }
                        }
                        if (accountId[0] === "UserId") {
                            displayName = result.issues[0].fields.assignee.displayName
                        }


                        dayNumber++;
                    }
                    this.setState({
                        isLoaded: true,
                        burnDownData: burnDownData,
                        displayName: displayName,
                        accountId: accountId
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, burnDownData, displayName, accountId} = this.state;

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            options.series[0].data = burnDownData;
            if (accountId[0] === "UserId") {
                options.series[0].name = displayName;
            } else {
                if (this.props.name === "Main") {
                    options.series[0].name = "All projects";
                } else {
                    options.series[0].name = this.props.name
                }
            }


            return (
                <div className="boxBurndownChart">

                    <div className="BurndownChart">
                        <a>Burndownchart</a>
                        <div className="BurndownChartData">
                            <HighchartsReact highcharts={Highcharts} options={options}/>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

//Burndownchart data, gegevens en opmaak
const options = {
    credits: {
        enabled: false
    },
    tooltip: {
        valueDecimals: 2,
        valueSuffix: '%'
    },
    legend: {
        itemStyle: {
            color: '#FFFFFF'
        }
    },
    xAxis: {

        categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
            'Sunday'],

        labels: {
            style: {
                color: '#FFFFFF'
            }
        }
    },

    yAxis: {
        labels: {
            style: {
                color: '#FFFFFF'
            }
        },
        min: 0,
        title: {
            //Data in procenten van volledige sprint
            text: 'Percentage'
        },
    },
    title: {
        text: null
    },
    series: [{
        name: 'My burn',
        data: [100, 100, 85, 75, 65, 40, 40, 35, 30, 25, 5, 0],
        color: 'white'
    }, {

        name: 'Ideal Burn',
        color: 'rgb(0, 0, 0, 0.75)',
        data: [100, 90, 80, 70, 60, 50, 50, 40, 30, 20, 10, 0]

    }]
}

//
export default burndownChart


