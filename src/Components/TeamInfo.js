import React, {Component} from "react";
import './Styling/TeamInfo.css'
import {Link} from "react-router-dom";
import APIGetCall from "./APIGetCall";
import PersonalData from './PersonalData.json';

class TeamInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }

    componentDidMount(props) {
        let Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/users")

        Call.fetchData()
            .then(response => response.json())
            .then((result) => {
                    this.setState({
                        isLoaded: true,
                        posts : result
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, posts} = this.state;
        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            let Users = [];
            //json bestand moet hie binnen komen
            for (let i = 0; i < posts.length; i++) {
                if(posts[i].accountType === "atlassian")
                    for(let j = 0; j < PersonalData.length; j++ ) {
                        if (posts[i].accountId === PersonalData[j].userId) {
                            let d = new Date();
                            let n = d.getDay();

                            Users.push({name: posts[i].displayName, accountID: posts[i].accountId, availability: PersonalData[j].availability[n], role: PersonalData[j].role /* hier moet user[i].data dat je wilt komen*/})
                        }
                    }



            }
            console.log(Users)

            return (
                <div className="container">
                    <div className="boxTeamInfo">
                        <table>
                            <tr>
                                <th>Name</th>
                                <th>Available</th>
                                <th>Role</th>
                            </tr>
                            {Users.map((item) => {
                                return (
                                    <tr className="border">
                                        {/*<td><ul className = "ulpadding"><li onClick="window.location.reload();"><Link to={"/PersonalPage/?UserName=" + item.name}><a>{item.name}</a></Link></li></ul></td>*/}
                                        <td><li onClick="window.location.reload();"><Link to={"/PersonalPage/?UserName=" + item.name}><a>{item.name}</a></Link></li></td>
                                        <td>{item.availability}</td>
                                        <td>{item.role}</td>
                                    </tr>
                                )
                            })}
                        </table>
                    </div>
                </div>

            )
        }
    }
}

export default TeamInfo

