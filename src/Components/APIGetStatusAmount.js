import React , { Component } from 'react';
import APIGetCall from "./APIGetCall";
class APIGetStatusAmount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }
//fking push die code goed.
    componentDidMount(props) {
        let Call;

        if(this.props.data[2] !== 0){
            Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints() and assignee in ("+ this.props.data[2] +") and status = \"" + this.props.data[0] + "\" ORDER BY Rank ASC");
        }
        else{
            if(this.props.data[1] === "Main"){
                Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=sprint in openSprints() AND  status = \"" + this.props.data[0] + "\" ORDER BY Rank ASC");
            }
            else{
                Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?jql=project = "+this.props.data[1]+" AND sprint in openSprints() AND  status = \"" + this.props.data[0] + "\" ORDER BY Rank ASC");
            }
        }
        Call.fetchData()
            .then(response => response.json())
            .then((result) => {
                    this.setState({
                        isLoaded: true,
                        posts: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, posts} = this.state;

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            let total
                total = posts.total;


            for (let i = 0; i < posts.issues.length; i++) {
                if (posts.issues[i].fields.aggregatetimeoriginalestimate != null) {
                    total += posts.issues[i].fields.aggregatetimeoriginalestimate;
                }
            }


            return (<h2>{Math.round((total/3600)*10) /10}</h2>)
        }

    }
}

export default APIGetStatusAmount