import React, {Component} from "react";
import './Styling/PersonalTickets.css'
import APIGetCall from "./APIGetCall";

class PersonalTickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: [],
            ticketArray: {},

        };
    }

    componentDidMount() {
        // let ticketArray = [];
        var ticketArray = [];

        let stats;

        let ticketUpdated;


        // console.log(this.props.name)

        let accountId = this.props.name.split("#");

        // console.log("AccountIDTickets= "+ accountId);


        if (this.props.name === "Main") {
            stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?filter=10000&jql=sprint in openSprints() AND status = Done")
        } else {
            stats = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/search?filter=10000&jql=sprint in openSprints() AND status = Done")
        }

        stats.fetchData()
            .then(response => response.json())
            .then((result) => {
                    let Total = 0;
                    for (let i = 0; i < result.issues.length; i++) {
                        if (result.issues[i].fields.assignee.accountId === accountId[1]) {

                            ticketUpdated = result.issues[i].fields.updated;
                            ticketUpdated = ticketUpdated.split('T')[0];


                            ticketArray.push([result.issues[i].fields.project.name,
                                result.issues[i].fields.summary,
                                ticketUpdated,
                                new Date(result.issues[i].fields.timeoriginalestimate * 1000).toISOString().substr(11, 8)
                            ])

                        }
                    }



                    this.setState({
                        isLoaded: true,
                        ticketArray: ticketArray
                    })

                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        const {error, isLoaded, posts, ticketArray} = this.state;
        Object.size = function (obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };

        // console.log(Object.size(ticketArray))

        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {

            let tickets = [];

            for (let i = 0; i < ticketArray.length; i++) {

                tickets.push({
                    ticketName: ticketArray[i][0],
                    ticketSummary: ticketArray[i][1],
                    ticketUpdated: ticketArray[i][2],
                    ticketEstimate: ticketArray[i][3]
                });
            }
            // console.log(tickets)

            return (
                <section className="containerPersonalTickets">
                    <div className="boxPersonalTickets">
                        <table className="ticketTable">
                            <tr>
                                <th>Sprint name</th>
                                <th>Ticket</th>
                                <th>Date</th>
                                <th>Estimated time</th>
                            </tr>
                            {tickets.map((item) => {
                                return (
                                    <tr className="border">
                                        <td>{item.ticketName}</td>
                                        <td className="ticketTwo">{item.ticketSummary}</td>
                                        <td>{item.ticketUpdated}</td>
                                        <td>{item.ticketEstimate}</td>
                                    </tr>
                                )
                            })}

                        </table>
                    </div>
                </section>

            )
        }
    }
}

export default PersonalTickets

