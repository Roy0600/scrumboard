import React, {Component} from 'react'
import '../Components/Styling/PersonalPage.css'
import '../Components/Styling/BurndownChart.css'
import ProgressStats from "../Components/ProgressStats";
import BurndownChart from "../Components/BurndownChart";
import ProgressBar from "../Components/ProgressBar";
import SprintGoal from "../Components/SprintGoal";
import TeamInfo from "../Components/TeamInfo";
import PersonalAvailability from "../Components/PersonalAvailability";
import PersonalTickets from "../Components/PersonalTickets";
import APIGetCall from "../Components/APIGetCall";


class PersonalPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            Username: null
        };
    }

//fking push die code goed.
    componentDidMount() {
        let Call = new APIGetCall("https://stark-hamlet-21132.herokuapp.com/https://semester2team3.atlassian.net/rest/api/2/users")
        console.log(this.props.data)
        const urlParams = new URLSearchParams(window.location.search);
        Call.fetchData()
            .then(response => response.json())
            .then((result) => {
                    this.setState({
                        isLoaded: true,
                        Username: urlParams.get('UserName'),
                        posts: result
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                },
            )
    }

    render() {
        require('../Components/Styling/PersonalStats.css');

        const {error, isLoaded, posts, Username} = this.state;
        let accountId;
        if (error) {
            return <div>error in loading</div>
        } else if (!isLoaded) {
            return <div>Loading.......</div>
        } else {
            console.log(Username)
            for (let i = 0; i < posts.length; i++) {
                if (posts[i].displayName === Username) {
                    accountId = posts[i].accountId;
                }
            }

            return (
                <div className='scaleabilityBodyPersonalPage'>
                    <div className='personalpage'>
                        <meta httpEquiv="refresh" content="300"/>
                        <div>
                            <div className="ScrumDashboardText">
                                <a>Dashboard     {Username}
                                {/*<br>*/}
                                {/*    <div>*/}
                                {/*        {<h1 style={{color: 'lightblue'}}>{Username}</h1>}*/}
                                {/*    </div>*/}
                                {/*</br>*/}

                                </a>

                                {/* <a>{"UserId:"+ accountId}</a>*/}
                            </div>
                        </div>

                        <div className="personalProgress">
                            <ProgressStats name={["0", accountId]}/>
                        </div>
                        <div className="personalsprintgoal">
                            <SprintGoal name={"UserId#" + accountId}/>
                            <PersonalAvailability userId={accountId}/>
                        </div>
                        <div>
                            <PersonalTickets name={"UserId#" + accountId}/>
                            <div className="personalBurndownChart">
                            <BurndownChart name={"UserId#" + accountId}/>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default PersonalPage