import React, {Component} from "react";
import ProgressBar from "../Components/ProgressBar";
import ProgressStats from "../Components/ProgressStats";
import BurndownChart from "../Components/BurndownChart";
import TeamInfo from "../Components/TeamInfo";
// import '../Components/Styling/Team.css'
import SprintGoal from "../Components/SprintGoal";


class Team extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }

    componentDidMount() {
        console.log("Props.data= ")
        console.log(this.props.data)
    }


    render() {
        require('../Components/Styling/Team.css');
        console.log(this.props.data.id)

        return (
            <div className='home'>
                <div className="ScrumDashboardText">
                    <a>{this.props.data.name}</a>
                </div>

                <ProgressBar name={this.props.data.key}/>
                <div className="teamprogressstats">
                    <ProgressStats name={[this.props.data.key, 0]}/>
                </div>

                <div className="sprintgoalteam">
                    <div>
                        {/*<div className="sprintGoal">*/}
                        <SprintGoal name={this.props.data.id} className="sprintGoal"/>
                        {/*</div>*/}
                        <div className="teamburndownchart">
                            <div>
                                <BurndownChart name={this.props.data.key}/>
                            </div>
                        </div>
                        <div className="teaminfojemoeder">
                        <TeamInfo/>
                        </div>
                    </div>
                </div>



            </div>
        );
    }

}

export default Team;