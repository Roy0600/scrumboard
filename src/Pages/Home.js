import React from 'react';
import  BurndownChart from '../Components/BurndownChart';
import ProgressStats from '../Components/ProgressStats';
import ProgressBar from '../Components/ProgressBar';
import APIGetCurrentSprint from "../Components/APIGetCurrentSprint";
import '../Components/Styling/App.css'

function Home () {
    return (
    <div className= 'scaleabilityBody'>
        <div className='home'>
            <meta httpEquiv="refresh" content="300"/>
            <div>
            <div className= "ScrumDashboardText">
                <a>Dashboard</a>
            </div>
            </div>
            <ProgressBar name = "Main"/>
            <BurndownChart name = "Main"/>
            <ProgressStats name ={["Main",0]}/>
        </div>
        </div>
    )
}

export default Home